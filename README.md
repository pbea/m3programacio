# Llenguatges de programació segons la proximitat al nucli de la màquina


En cada pàgina es tracta un **nivell** diferent de proximitat al nucli:

[Baix nivell](p1.md)

[Nivell assemblador](p2.md)

[Llenguatges de sistemes](p3.md)

[Llenguatges d'alt nivell](p4.md)

